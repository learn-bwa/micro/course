<?php

namespace App\Http\Controllers;

use App\Models\Chapter;
use App\Models\Course;
use App\Models\Mentor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChapterController extends Controller
{
    public function index(Request $request)
    {
        $chapter = Chapter::query();

        $courseId = $request->query('course_id');
        $chapter->when($courseId, function ($query) use ($courseId) {
            return $query->where('course_id', $courseId);
        });

        return response()->json([
            'status' => 'success',
            'data' => $chapter->get()
        ]);
    }

    public function create(Request $request)
    {
        $rule = [
            'name' => 'required|string',
            'course_id' => 'required|int',
        ];

        $data = $request->all();

        $validate = Validator::make($data, $rule);
        if ($validate->fails())
        {
            return response()->json([
                'status' => 'error',
                'message' => $validate->errors()
            ], 400);
        }

        $courseId = $request->input('course_id');
        $course = Course::find($courseId);
        if (!$course) {
            return response()->json([
                'status' => 'error',
                'message' => 'course not found'
            ], 404);
        }

        $course = Chapter::create($data);
        return response()->json([
            'status' => 'success',
            'data' => $course
        ]);
    }

    public function show($id)
    {
        $chapter = Chapter::find($id);
        if (!$chapter) {
            return response()->json([
                'status' => 'error',
                'message' => 'chapter not found'
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'data' => $chapter
        ]);
    }

    public function update(Request $request, $id)
    {
        $rule = [
            'name' => 'string',
            'course_id' => 'int'
        ];
        $data = $request->all();

        $validate = Validator::make($data, $rule);
        if ($validate->fails())
        {
            return response()->json([
                'status' => 'error',
                'message' => $validate->errors()
            ], 400);
        }

        $courseId = $request->input('course_id');
        $course = Course::find($courseId);
        if (!$course) {
            return response()->json([
                'status' => 'error',
                'message' => 'course not found'
            ], 404);
        }


        $chapter = Chapter::find($id);
        if (!$chapter) {
            return response()->json([
                'status' => 'error',
                'message' => 'chapter not found'
            ], 404);
        }

        $chapter->fill($data);
        $chapter->save();

        return response()->json([
            'status' => 'success',
            'data' => $chapter
        ]);
    }

    public function destroy($id)
    {
        $chapter = Chapter::find($id);
        if (!$chapter) {
            return response()->json([
                'status' => 'error',
                'message' => 'chapter not found'
            ]);
        }

        $chapter->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'chapter deleted successfully',
            'data' => null,
        ]);
    }
}
