<?php

namespace App\Http\Controllers;

use App\Models\Chapter;
use App\Models\Course;
use App\Models\Mentor;
use App\Models\MyCourse;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    public function index(Request $request)
    {
        $courses = Course::query();
        
        $q = $request->query('q');
        $status =$request->query('status');

        $courses->when($q, function ($query) use ($q) {
            return $query->whereRaw('name like "%'.strtolower($q).'%"');
        })
        ->when($status, function ($query) use ($status) {
            return $query->where('status', $status);
        });

        return response()->json([
            'status' => 'success',
            'data' => $courses->paginate(10)
        ]);
    }

    public function create(Request $request)
    {
        $rule = [
            'name' => 'required|string',
            'certificate' => 'required|boolean',
            'thumbnail' => 'url',
            'type' => 'required|in:free,premuim',
            'status' => 'required|in:draft,published',
            'price' => 'integer',
            'level' => 'required|in:all-lavel,beginner,intermediate,advance',
            'mentor_id' => 'required|integer',
            'description' => 'required|string',
        ];
        $data = $request->all();

        $validate = Validator::make($data, $rule);
        if ($validate->fails())
        {
            return response()->json([
                'status' => 'error',
                'message' => $validate->errors()
            ], 400);
        }

        $mentoId = $request->input('mentor_id');
        $mentor = Mentor::find($mentoId);
        if (!$mentor) {
            return response()->json([
                'status' => 'error',
                'message' => 'mentor not found'
            ], 404);
        }

        $course = Course::create($data);
        return response()->json([
            'status' => 'success',
            'data' => $course
        ]);
    }

    public function update(Request $request, $id)
    {
        $rule = [
            'name' => 'string',
            'certificate' => 'boolean',
            'thumbnail' => 'url',
            'type' => 'in:free,premuim',
            'status' => 'in:draft,published',
            'price' => 'integer',
            'level' => 'in:all-lavel,beginner,intermediate,advance',
            'mentor_id' => 'integer',
            'description' => 'string',
        ];
        $data = $request->all();

        $validate = Validator::make($data, $rule);
        if ($validate->fails())
        {
            return response()->json([
                'status' => 'error',
                'message' => $validate->errors()
            ], 400);
        }

        $mentoId = $request->input('mentor_id');
        
        if ($mentoId && !$mentor = Mentor::find($mentoId)) {
            return response()->json([
                'status' => 'error',
                'message' => 'mentor not found'
            ], 404);
        }

        $course = Course::find($id);
        if (!$course) {
            return response()->json([
                'status' => 'error',
                'message' => 'course not found'
            ], 404);
        }

        $course->fill($data);
        $course->save();

        return response()->json([
            'status' => 'success',
            'data' => $course
        ]);
    }

    public function destroy($id)
    {
        $course = Course::find($id);
        if (!$course) {
            return response()->json([
                'status' => 'error',
                'message' => 'course not found'
            ]);
        }

        $course->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'course deleted successfully',
            'data' => null,
        ]);
    }

    public function show($id)
    {
        $course = Course::with('chapters.lessons', 'mentor', 'images')->find($id);
        if (!$course) {
            return response()->json([
                'status' => 'error',
                'message' => 'course not found'
            ]);
        }

        $reviews = Review::where('course_id', $course->id)->get()->toArray();
        $totalStudent = MyCourse::where('course_id', $course->id)->count();
        $totalVideos = Chapter::where('course_id', $id)->withCount('lessons')->get()->toArray();
        $finalTotalVideos = array_sum(array_column($totalVideos, 'lessons_count'));

        if (count($reviews) > 0) {
            $userIds = array_column($reviews, 'user_id');
            $users = getUserByIds($userIds);
            if ($users['status'] == 'error') {
                $reviews = [];
            } else {
                foreach ($reviews as $key => $review) {
                    $userIndex = array_search($review['user_id'], $users['data']);
                    $review[$key]['users'] = $users['data'][$userIndex];
                }
            }
        }

        $course['reviews'] = $reviews;
        $course['total_videos'] = $finalTotalVideos;
        $course['total_student'] = $totalStudent;

        return response()->json([
            'status' => 'success',
            'data' => $course,
        ]);
    }
}
