<?php

namespace App\Http\Controllers;

use App\Models\Chapter;
use App\Models\Course;
use App\Models\Lesson;
use App\Models\Mentor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LessonController extends Controller
{
    public function index(Request $request)
    {
        $lessons = Lesson::query();

        $chapteId = $request->query('chapter_id');
        $lessons->when($chapteId, function ($query) use ($chapteId) {
            return $query->where('chapter_id', $chapteId);
        });

        return response()->json([
            'status' => 'success',
            'data' => $lessons->get()
        ]);
    }

    public function create(Request $request)
    {
        $rule = [
            'name' => 'required|string',
            'video' => 'required|string',
            'chapter_id' => 'required|int',
        ];

        $data = $request->all();

        $validate = Validator::make($data, $rule);
        if ($validate->fails())
        {
            return response()->json([
                'status' => 'error',
                'message' => $validate->errors()
            ], 400);
        }

        $chapterId = $request->input('chapter_id');
        $chapter = Chapter::find($chapterId);
        if (!$chapter) {
            return response()->json([
                'status' => 'error',
                'message' => 'chapter not found'
            ], 404);
        }

        $lesson = Lesson::create($data);
        return response()->json([
            'status' => 'success',
            'data' => $lesson
        ]);
    }

    public function show($id)
    {
        $lesson = Lesson::find($id);
        if (!$lesson) {
            return response()->json([
                'status' => 'error',
                'message' => 'lesson not found'
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'data' => $lesson
        ]);
    }

    public function update(Request $request, $id)
    {
        $rule = [
            'name' => 'required|string',
            'video' => 'required|string',
            'chapter_id' => 'required|int',
        ];

        $data = $request->all();

        $validate = Validator::make($data, $rule);
        if ($validate->fails())
        {
            return response()->json([
                'status' => 'error',
                'message' => $validate->errors()
            ], 400);
        }

        $chapterId = $request->input('chapter_id');
        $course = Chapter::find($chapterId);
        if (!$course) {
            return response()->json([
                'status' => 'error',
                'message' => 'chapter not found'
            ], 404);
        }


        $lesson = Lesson::find($id);
        if (!$lesson) {
            return response()->json([
                'status' => 'error',
                'message' => 'lesson not found'
            ], 404);
        }

        $lesson->fill($data);
        $lesson->save();

        return response()->json([
            'status' => 'success',
            'data' => $lesson
        ]);
    }

    public function destroy($id)
    {
        $lesson = Lesson::find($id);
        if (!$lesson) {
            return response()->json([
                'status' => 'error',
                'message' => 'lesson not found'
            ]);
        }

        $lesson->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'lesson deleted successfully',
            'data' => null,
        ]);
    }
}
