<?php

namespace App\Http\Controllers;

use App\Models\Chapter;
use App\Models\Course;
use App\Models\ImageCourse;
use App\Models\Lesson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ImageCourseController extends Controller
{
    public function index(Request $request)
    {
        $imageCourses = ImageCourse::query();

        $courseId = $request->query('course_id');
        $imageCourses->when($courseId, function ($query) use ($courseId) {
            return $query->where('course_id', $courseId);
        });

        return response()->json([
            'status' => 'success',
            'data' => $imageCourses->get()
        ]);
    }

    public function create(Request $request)
    {
        $rule = [
            'image' => 'required|url',
            'course_id' => 'required|int',
        ];

        $data = $request->all();

        $validate = Validator::make($data, $rule);
        if ($validate->fails())
        {
            return response()->json([
                'status' => 'error',
                'message' => $validate->errors()
            ], 400);
        }

        $courseId = $request->input('course_id');
        $course = Course::find($courseId);
        if (!$course) {
            return response()->json([
                'status' => 'error',
                'message' => 'course not found'
            ], 404);
        }

        $imageCourse = ImageCourse::create($data);
        return response()->json([
            'status' => 'success',
            'data' => $imageCourse
        ]);
    }

    public function show($id)
    {
        $imageCourse = ImageCourse::find($id);
        if (!$imageCourse) {
            return response()->json([
                'status' => 'error',
                'message' => 'image course not found'
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'data' => $imageCourse
        ]);
    }

    public function update(Request $request, $id)
    {
        $rule = [
            'image' => 'required|url',
            'course_id' => 'required|int',
        ];

        $data = $request->all();

        $validate = Validator::make($data, $rule);
        if ($validate->fails())
        {
            return response()->json([
                'status' => 'error',
                'message' => $validate->errors()
            ], 400);
        }

        $courseId = $request->input('course_id');
        $course = Course::find($courseId);
        if (!$course) {
            return response()->json([
                'status' => 'error',
                'message' => 'course not found'
            ], 404);
        }


        $imageCourse = ImageCourse::find($id);
        if (!$imageCourse) {
            return response()->json([
                'status' => 'error',
                'message' => 'image course not found'
            ], 404);
        }

        $imageCourse->fill($data);
        $imageCourse->save();

        return response()->json([
            'status' => 'success',
            'data' => $imageCourse
        ]);
    }

    public function destroy($id)
    {
        $imageCourse = ImageCourse::find($id);
        if (!$imageCourse) {
            return response()->json([
                'status' => 'error',
                'message' => 'image course not found'
            ]);
        }

        $imageCourse->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'image course deleted successfully',
            'data' => null,
        ]);
    }
}
